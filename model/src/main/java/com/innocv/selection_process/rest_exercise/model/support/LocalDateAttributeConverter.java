package com.innocv.selection_process.rest_exercise.model.support;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import java.sql.Date;
import java.time.LocalDate;


/**
 * Defines the class of converters capable of converting between {@link LocalDate} and {@link Date}.
 *
 * @author INNOCV.
 */
@Convert()
public class LocalDateAttributeConverter implements AttributeConverter< LocalDate, Date >
{

    @Override
    public Date convertToDatabaseColumn( LocalDate attribute )
    {

        return attribute == null ? null : Date.valueOf( attribute );
    }


    @Override
    public LocalDate convertToEntityAttribute( Date dbData )
    {

        return dbData == null ? null : dbData.toLocalDate();
    }
}
