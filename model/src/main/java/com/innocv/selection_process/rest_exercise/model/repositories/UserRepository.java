package com.innocv.selection_process.rest_exercise.model.repositories;

import com.innocv.selection_process.rest_exercise.model.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;


/**
 * Repository for the {@link User} entity.
 *
 * @author INNOCV
 */
@Transactional( readOnly = true )
public interface UserRepository extends JpaRepository< User, Integer > {


    /**
     * Returns the users whose name is equal to the specified <code>name</code>.
     *
     * @param name  some name.
     * @return a possibly empty list of users.
     */
    List< User > findByName( @Param("name") String name );


    /**
     * Returns the users whose birth date is equal to the specified <code>birthDate</code>.
     *
     * @param birthDate  a birth date.
     * @return a possibly empty list of users.
     */
    List< User > findByBirthDate( @Param( "birthDate" ) LocalDate birthDate );


    @Query( "select count( us.id ) from User us where us.id = :id" )
    Integer countById( @Param( "id" ) Integer id );

}
