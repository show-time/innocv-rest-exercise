package com.innocv.selection_process.rest_exercise.model;


/**
 * Defines constants that span the whole model.
 *
 * @author INNOCV.
 */
public class Constants {


    /**
     * Name of the persistence unit of the model.
     *
     */
    public static final String PERSISTENCE_UNIT_NAME = "rest-exercise";

    /**
     * Base package for entities. Helps in defining the scope of entity scan.
     *
     */
    public static final String ENTITIES_BASE_PACKAGE = "com.innocv.selection_process.rest_exercise.model.domain";

    /**
     * Base package for repositories. Helps in defining the scope of repository scan.
     *
     */
    public static final String REPOSITORIES_BASE_PACKAGE = "com.innocv.selection_process.rest_exercise.model.repositories";

}
