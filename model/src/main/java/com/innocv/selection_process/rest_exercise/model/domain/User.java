package com.innocv.selection_process.rest_exercise.model.domain;

import com.innocv.selection_process.rest_exercise.model.support.LocalDateAttributeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


/**
 * Represents persistent users.
 *
 * @author InnoCV
 */
@Data
@Entity
@EqualsAndHashCode( of = "id" )
public class User implements Serializable
{

    private static final long serialVersionUID = 1882518905247549649L;


    /**
     * Tells the id of this user.
     *
     */
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Integer id;


    /**
     * Tells the name of this user.
     *
     */
    @Column( nullable = false )
    private String name;


    /**
     * Tells the birth date of this user.
     *
     */
    @Column( nullable = false)
    @Convert( converter = LocalDateAttributeConverter.class )
    private LocalDate birthDate;

}
