package com.innocv.selection_process.rest_exercise.model.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Java6Assertions.assertThat;


/**
 * Tests for the {@link User} class.
 *
 * @author INNOCV.
 */
@RunWith( SpringRunner.class )
@DataJpaTest
public class UserTests {


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private TestEntityManager testEntityManager;


    @Configuration
    public static class CustomTestConfiguration {
        //do not remove this class; it looks unnecesary but it actually is necessary
    }


    @Test
    public void saveShouldPersistData() throws Exception {

        //let's prepare the test filesystem
        User expected = new User();

        expected.setName( "some name" );

        expected.setBirthDate( LocalDate.now() );

        User user = this.testEntityManager.persistFlushFind( expected );

        assertThat( user.getId() ).isEqualTo( expected.getId() );

        assertThat( user.getName() ).isEqualTo( expected.getName() );

        assertThat( user.getBirthDate() ).isEqualTo( expected.getBirthDate() );

    }

}
