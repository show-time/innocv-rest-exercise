package com.innocv.selection_process.rest_exercise.model.repositories;

import com.innocv.selection_process.rest_exercise.model.Constants;
import com.innocv.selection_process.rest_exercise.model.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Java6Assertions.assertThat;


/**
 * Tests for the {@link UserRepository}.
 *
 * @author INNOCV
 */
@RunWith( SpringRunner.class )
@DataJpaTest
public class UserRepositoryTests {


    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;


    @Configuration
    @EntityScan( basePackages = Constants.ENTITIES_BASE_PACKAGE )
    @EnableJpaRepositories( basePackages = Constants.REPOSITORIES_BASE_PACKAGE )
    public static class CustomTestConfiguration {
    }


    @Test
    public void findByNameShouldReturnMatchingUsers() {

        String name = "some name";

        User user = new User();

        user.setName( name );

        user.setBirthDate( LocalDate.now() );

        this.entityManager.persist( user );

        assertThat( this.userRepository.findByName( name ) ).containsOnly( user );
    }


    @Test
    public void findByNameWhenNameDoesNotMatchShouldReturnEmptyCollectionOfUsers() {

        String name = "some name";

        User user = new User();

        user.setName( name );

        user.setBirthDate( LocalDate.now() );

        this.entityManager.persist( user );

        assertThat( this.userRepository.findByName( "some other name" ) ).isEmpty();
    }


    @Test
    public void findByBirthDateShouldReturnMatchingUsers() {

        String name = "some name";

        LocalDate birthDate = LocalDate.now();

        User user = new User();

        user.setName( name );

        user.setBirthDate( birthDate );

        this.entityManager.persist( user );

        assertThat( this.userRepository.findByBirthDate( birthDate ) ).containsOnly( user );
    }


    @Test
    public void findByBirthDateWhenBirthDateDoesNotMatchShouldReturnEmptyCollectionOfUsers() {

        String name = "some name";

        LocalDate birthDate = LocalDate.now();

        User user = new User();

        user.setName( name );

        user.setBirthDate( birthDate );

        this.entityManager.persist( user );

        assertThat( this.userRepository.findByBirthDate( birthDate.minusDays( 1 ) ) ).isEmpty();
    }


}
