package com.innocv.selection_process.rest_exercise.users;


import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Represents the fact that a an action over a user has been requested, but such a user can not be identified with the
 * userId in question.
 *
 * @author INNOCV.
 */
@ResponseStatus( code = HttpStatus.NOT_FOUND, reason = "no user with the specified userId is found" )
public class NoSuchUserException extends RuntimeException {


    @Getter
    private final Integer userId;


    public NoSuchUserException(Integer userId ) {

        Assert.notNull( userId, "userId must not be null" );

        this.userId = userId;
    }

}
