package com.innocv.selection_process.rest_exercise.aop;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Defines common-to-all-controllers error handling advice rules.
 *
 * <p>The purpose of this exception handler is to setup exception handling policies common to all controllers; like
 * turning instances of {@link IllegalArgumentException} into <code>BAD REQUEST</code> HTTP status codes.</p>
 *
 * @author INNOCV.
 */
@ControllerAdvice
public class CommonExceptionHandlingAdvice {

    /**
     * Installs an exception handler for all controller methods which turns <code>IllegalArgumentExceptions</code>
     * into <code>400 Bad Request</code> status codes.
     *
     */
    @ExceptionHandler( IllegalArgumentException.class )
    public void handleIllegalArgumentExceptions( HttpServletResponse response, IllegalArgumentException e ) throws IOException {

        response.sendError( HttpStatus.BAD_REQUEST.value(), e.getMessage() );
    }

}
