package com.innocv.selection_process.rest_exercise;


/**
 * Defines constants that span the whole application.
 *
 * @author INNOCV.
 */
public class Constants
{

    /**
     * Base mapping for all api end points.
     *
     */
    public static final String BASE_API_ENDPOINT = "/api";
}
