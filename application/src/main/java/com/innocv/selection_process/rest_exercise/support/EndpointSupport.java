package com.innocv.selection_process.rest_exercise.support;

import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Map;

import static java.util.stream.Collectors.joining;

/**
 * Collects several utility methods for path variables management.
 *
 * @author INNOCV.
 */
public class EndpointSupport {


    public static final String PATH_SEPARATOR = "/";

    public static final String LEFT_CURLY_BRACE = "{";

    public static final String RIGHT_CURLY_BRACE = "}";


    /**
     * Same as calling <code>resolvePathVariables( endpointExpression, pathVariables, true )</code>.
     *
     * @param endpointExpression  some resolvePathVariables expression.
     * @param pathVariables  a series of path variable name-value mappings.
     * @return  the resolved resolvePathVariables.
     */
    public static String resolvePathVariables( String endpointExpression, Map< String, String > pathVariables ) {

        Assert.hasText( endpointExpression, "endpointExpression must not be null nor empty" );

        Assert.notNull( pathVariables, "pathVariables must not be null" );

        return Arrays
                .stream( endpointExpression.split( PATH_SEPARATOR ) )
                .map( token -> clearBraces( token ) )
                .map( token -> pathVariables.getOrDefault( token, token ) )
                .map( token -> token != null ? token.replace( " ",  "%20" ):token )
                .collect( joining( PATH_SEPARATOR ) );
    }


    private static String clearBraces( String token ) {

        String modifiedToken = token;

        if( modifiedToken.startsWith( LEFT_CURLY_BRACE ) && modifiedToken.length() > 1 ) {

            modifiedToken = modifiedToken.substring( 1 );
        }

        if( modifiedToken.endsWith( RIGHT_CURLY_BRACE ) ) {

            modifiedToken = modifiedToken.substring( 0, modifiedToken.length() - 1 );
        }

        return modifiedToken;
    }

}
