package com.innocv.selection_process.rest_exercise.config;

import com.innocv.selection_process.rest_exercise.model.Constants;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/**
 * Defines the app configuration.
 *
 * <p>Use this class to enable configuration properties, perform configuration checks, and all the stuff needed to
 * wire your service.</p>
 *
 * @author INNOCV.
 */
@Configuration
@EntityScan( basePackages = Constants.ENTITIES_BASE_PACKAGE )
@EnableJpaRepositories( basePackages = Constants.REPOSITORIES_BASE_PACKAGE )
public class AppConfiguration {


}
