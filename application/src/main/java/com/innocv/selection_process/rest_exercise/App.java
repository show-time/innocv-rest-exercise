package com.innocv.selection_process.rest_exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Defines the main entry point to your app.
 *
 * @author INNOCV.
 */
@SpringBootApplication
public class App {


    public static void main( String... args ) {

        SpringApplication.run( App.class, args );
    }
}
