package com.innocv.selection_process.rest_exercise.users;


import com.innocv.selection_process.rest_exercise.Constants;
import com.innocv.selection_process.rest_exercise.model.domain.User;
import com.innocv.selection_process.rest_exercise.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


/**
 * Defines the api for user management.
 *
 * @author INNOCV.
 */
@RestController
@RequestMapping(
        value = UserRestController.USER_API_ENDPOINT
)
public class UserRestController {

    /**
     * Endpoint prefix for all user api endpoints.
     *
     */
    public static final String USER_API_ENDPOINT = Constants.BASE_API_ENDPOINT + "/user";

    /**
     * The <code>getall</code> endpoint.
     *
     */
    public static final String USER_API_GETALL_ENDPOINT = "/getall";

    /**
     * The <code>get/{id}</code> endpoint.
     *
     */
    public static final String USER_API_GET_ID_ENDPOINT = "/get/{id}";

    /**
     * The <code>create</code> endpoint.
     */
    public static final String USER_API_CREATE_ENDPOINT = "/create";

    /**
     * The <code>upddate</code> endpoint.
     */
    public static final String USER_API_UPDATE_ENDPOINT = "/update";

    /**
     * The <code>remove/{id}</code>
     */
    public static final String USER_API_REMOVE_ENDPOINT = "/remove/{id}";


    private final UserService userService;


    /**
     * Creates an instance of this class equipped with the specified <code>userService</code>.
     *
     * @param userService  a user service.
     */
    public UserRestController( UserService userService ) {

        this.userService = userService;
    }


    @GetMapping( USER_API_GETALL_ENDPOINT )
    public List< User > getAll() {

        return this.userService.findAll();
    }


    @GetMapping( USER_API_GET_ID_ENDPOINT )
    public User get( @PathVariable Integer id ) {

        Optional< User > optionalUser = this.userService.findOne( id );

        return optionalUser.orElseThrow( () -> new NoSuchUserException( id ) );
    }


    @DeleteMapping( USER_API_REMOVE_ENDPOINT )
    public void remove( @PathVariable Integer id ) {

        Optional< Integer > optionalUserId = this.userService.removeUser( id );

        if( !optionalUserId.isPresent() ) {

            throw new NoSuchUserException( id );
        }
    }


    @PostMapping( USER_API_CREATE_ENDPOINT )
    public User create( @RequestBody User user ) {

        return this.userService.createOrUpdateUser( user );
    }


    @PutMapping( USER_API_UPDATE_ENDPOINT )
    public User update( @RequestBody User user ) {

        return this.userService.createOrUpdateUser( user );
    }


}
