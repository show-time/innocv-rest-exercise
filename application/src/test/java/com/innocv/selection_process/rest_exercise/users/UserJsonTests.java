package com.innocv.selection_process.rest_exercise.users;

import com.innocv.selection_process.rest_exercise.model.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Json tests for the {@link User} class.
 *
 * @author INNOCV.
 */
@RunWith( SpringRunner.class )
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE,
        properties = { "spring.config.location=classpath:" + UserJsonTests.CONFIG_FILE }
)
@AutoConfigureJson
@AutoConfigureJsonTesters
public class UserJsonTests
{


    public static final String CONFIG_FILE = "/com/innocv/selection_process/rest_exercise/users/user.yml";


    @Autowired
    private JacksonTester< User > json;


    @Configuration
    public static class UserJsonTestsConfiguration {
    }


    @Test
    public void serializeJson() throws Exception
    {

        User user = new User();

        user.setId( 1234 );

        user.setName( "some name" );

        user.setBirthDate( LocalDate.of( 2017, 3, 27 ) );

        assertThat( this.json.write( user ) ).isEqualToJson( "user.json" );
    }


    @Test
    public void deserializeJson() throws Exception
    {

        User user = new User();

        user.setId( 1234 );

        user.setName( "some name" );

        user.setBirthDate( LocalDate.of( 2017, 3, 27 ) );

        assertThat( this.json.readObject("user.json") ).isEqualTo( user );
    }

}
