package com.innocv.selection_process.rest_exercise.users;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innocv.selection_process.rest_exercise.model.domain.User;
import com.innocv.selection_process.rest_exercise.service.UserService;
import com.innocv.selection_process.rest_exercise.support.EndpointSupport;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Tests for the {@link UserRestController} class.
 *
 * @author INNOCV.
 */
@RunWith( SpringRunner.class )
@WebMvcTest( UserRestController.class )
public class UserRestControllerTests {


    @Autowired
    private MockMvc mvc;

    @Autowired
    private ApplicationContext applicationContext;

    @MockBean
    private UserService userService;


    @Test
    public void getAllShouldReturnAllUsers() throws Exception {

        // let's prepare the conditions
        List< User > expected = Lists.newArrayList();

        given( this.userService.findAll() ).willReturn( expected );

        // let's prepare the call
        String url = UserRestController.USER_API_ENDPOINT + UserRestController.USER_API_GETALL_ENDPOINT;

        // let's perform the call and assert
        ObjectMapper objectMapper = new ObjectMapper();

        this.mvc.perform(
                get( url )
                .accept( MediaType.APPLICATION_JSON )
        )
        .andExpect( status().isOk() )
        .andExpect( content().json( objectMapper.writeValueAsString( expected ) ) );
    }


    @Test
    public void getWhenIdIsUnknownShouldReturnNotFound() throws Exception {

        // let's prepare the conditions
        Integer userId = 1;

        given( this.userService.findOne( userId ) ).willReturn( Optional.empty() );

        // let's prepare the call
        Map< String, String > pathVariables = new HashMap<>();

        pathVariables.put( "id", userId.toString() );

        String url = EndpointSupport.resolvePathVariables( UserRestController.USER_API_ENDPOINT +
                UserRestController.USER_API_GET_ID_ENDPOINT, pathVariables );

        // let's perform the call and assert
        this.mvc.perform(
                get( url )
                .accept( MediaType.APPLICATION_JSON )
        ).andExpect( status().isNotFound() );
    }


    @Test
    public void getWhenIdIsKnownShouldReturnUserAsJson() throws Exception {

        // let's prepare the conditions
        Integer userId = 1234;

        User user = new User();

        user.setId( userId );

        user.setName( "some name");

        user.setBirthDate( LocalDate.of( 2017, 3, 27 ) );

        given( this.userService.findOne( userId ) ).willReturn( Optional.of( user ) );

        // let's prepare the call
        Map< String, String > pathVariables = new HashMap<>();

        pathVariables.put( "id", userId.toString() );

        String url = EndpointSupport.resolvePathVariables( UserRestController.USER_API_ENDPOINT +
                UserRestController.USER_API_GET_ID_ENDPOINT, pathVariables );

        ClassPathResource cpr = new ClassPathResource( UserRestController.class.getPackage().getName().replace(".", "/" ) + "/user.json" );

        String userJson = new String( Files.readAllBytes( Paths.get( cpr.getURI() ) ) );

        this.mvc.perform(
                get( url )
                .accept( MediaType.APPLICATION_JSON )
        )
        .andExpect( status().isOk() )
        .andExpect( content().json( userJson ) );
    }


    @Test
    public void removeWhenIdIsUnknownShouldReturnNotFound() throws Exception {

        // let's prepare the conditions
        Integer userId = 1;

        given( this.userService.removeUser( userId ) ).willReturn( Optional.empty() );

        // let's prepare the call
        Map< String, String > pathVariables = new HashMap<>();

        pathVariables.put( "id", userId.toString() );

        String url = EndpointSupport.resolvePathVariables( UserRestController.USER_API_ENDPOINT +
                UserRestController.USER_API_REMOVE_ENDPOINT, pathVariables );

        // let's perform the call and assert
        this.mvc.perform(
                delete( url )
        )
        .andExpect( status().isNotFound() );
    }


    @Test
    public void removeWhenIdIsKnownShouldReturnUserAsJson() throws Exception {

        // let's prepare the conditions
        Integer userId = 1;

        User user = new User();

        user.setId( userId );

        user.setName( "innocv");

        user.setBirthDate( LocalDate.now() );

        given( this.userService.removeUser( userId ) ).willReturn( Optional.of( userId ) );

        // let's prepare the call
        Map< String, String > pathVariables = new HashMap<>();

        pathVariables.put( "id", userId.toString() );

        String url = EndpointSupport.resolvePathVariables( UserRestController.USER_API_ENDPOINT +
                UserRestController.USER_API_REMOVE_ENDPOINT, pathVariables );

        this.mvc.perform(
                delete( url )
        )
        .andExpect( status().isOk() );
    }


    @Test
    public void createWhenUserIsNullShouldReturnBadRequest() throws Exception {

        // let's prepare the call
        String url = UserRestController.USER_API_ENDPOINT + UserRestController.USER_API_CREATE_ENDPOINT;

        // let's perform the call
        this.mvc.perform(
                post( url )
                .accept( MediaType.APPLICATION_JSON )
                .contentType( MediaType.APPLICATION_JSON )
                .content( "null" )
        ).andExpect( status().isBadRequest() );
    }


    @Test
    public void createWhenUserIsValidShouldReturnCreatedUser() throws Exception {

        // let's prepare test data
        User user = new User();

        user.setName( "some name");

        user.setBirthDate( LocalDate.of( 2017, 3, 27 ) );

        User newUser = new User();

        newUser.setId( 1 );

        newUser.setName( user.getName() );

        newUser.setBirthDate( user.getBirthDate() );

        given( this.userService.createOrUpdateUser( user ) ).willReturn( newUser );

        // let's prepare the call
        String url = UserRestController.USER_API_ENDPOINT + UserRestController.USER_API_CREATE_ENDPOINT;

        ClassPathResource newUserResource = new ClassPathResource( UserRestController.class.getPackage().getName().replace(".", "/" ) + "/new-user.json" );

        String newUserJson = new String( Files.readAllBytes( Paths.get( newUserResource.getURI() ) ) );

        ClassPathResource createUserResource = new ClassPathResource( UserRestController.class.getPackage().getName().replace(".", "/" ) + "/create-user.json" );

        String createUserJson = new String( Files.readAllBytes( Paths.get( createUserResource.getURI() ) ) );

        // let's perform the call and assert
        this.mvc.perform(
                post( url )
                .accept( MediaType.APPLICATION_JSON )
                .contentType( MediaType.APPLICATION_JSON )
                .content( createUserJson )
        )
        .andExpect( status().isOk() )
        .andExpect( content().json( newUserJson ) );
    }


    @Test
    public void updateWhenUserIsNullShouldReturnBadRequest() throws Exception {

        // let's prepare the call
        String url = UserRestController.USER_API_ENDPOINT + UserRestController.USER_API_UPDATE_ENDPOINT;

        // let's perform the call
        this.mvc.perform(
                put( url )
                        .accept( MediaType.APPLICATION_JSON )
                        .contentType( MediaType.APPLICATION_JSON )
                        .content( "null" )
        ).andExpect( status().isBadRequest() );
    }


    @Test
    public void updateWhenUserIsValidShouldReturnUpdatedUser() throws Exception {

        // let's prepare test data
        User updateUser = new User();

        updateUser.setId( 1 );

        updateUser.setName( "some name");

        updateUser.setBirthDate( LocalDate.of( 2017, 3, 27 ) );

        User updatedUser = new User();

        updatedUser.setId( 1 );

        updatedUser.setName( updateUser.getName() );

        updatedUser.setBirthDate( updateUser.getBirthDate() );

        given( this.userService.createOrUpdateUser( updateUser ) ).willReturn( updatedUser );

        // let's prepare the call
        String url = UserRestController.USER_API_ENDPOINT + UserRestController.USER_API_UPDATE_ENDPOINT;

        ClassPathResource updateUserResource = new ClassPathResource( UserRestController.class.getPackage().getName().replace(".", "/" ) + "/update-user.json" );

        String updateUserJson = new String( Files.readAllBytes( Paths.get( updateUserResource.getURI() ) ) );

        // let's perform the call and assert
        this.mvc.perform(
                put( url )
                        .accept( MediaType.APPLICATION_JSON )
                        .contentType( MediaType.APPLICATION_JSON )
                        .content( updateUserJson )
        )
                .andExpect( status().isOk() )
                .andExpect( content().json( updateUserJson ) );
    }


}
