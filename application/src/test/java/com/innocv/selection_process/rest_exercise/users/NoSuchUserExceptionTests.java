package com.innocv.selection_process.rest_exercise.users;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Tests for the {@link NoSuchUserException} class.
 *
 * @author INNOCV.
 */
public class NoSuchUserExceptionTests
{


    @Rule public ExpectedException thrown = ExpectedException.none();


    @Test
    public void constructorWhenUserIdIsNullShouldThrowException() {

        this.thrown.expect( IllegalArgumentException.class );

        new NoSuchUserException( null );
    }

    @Test
    public void constructorShouldConstruct() {

        Integer userId = 1;

        NoSuchUserException noSuchUserException = new NoSuchUserException( userId );

        assertThat( noSuchUserException.getUserId() ).isEqualTo( userId );
    }

}
