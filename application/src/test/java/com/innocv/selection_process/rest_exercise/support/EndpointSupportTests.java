package com.innocv.selection_process.rest_exercise.support;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;


/**
 * Tests for the {@link EndpointSupport} class.
 *
 * @author INNOCV.
 */
public class EndpointSupportTests {


    @Rule public ExpectedException thrown = ExpectedException.none();


    @Test
    public void resolvePathVariablesWhenEndpointExpressionIsNullShouldThrowException() {

        this.thrown.expect( IllegalArgumentException.class );

        EndpointSupport.resolvePathVariables( null, new HashMap<>() );
    }


    @Test
    public void resolvePathVariablesWhenEndpointExpressionHasNoTextShouldThrowException() {

        this.thrown.expect( IllegalArgumentException.class );

        EndpointSupport.resolvePathVariables( "", new HashMap<>() );
    }


    @Test
    public void resolvePathVariablesWhenParametersIsNullShouldThrowException() {

        this.thrown.expect( IllegalArgumentException.class );

        EndpointSupport.resolvePathVariables( "/ep", null );
    }


    @Test
    public void resolvePathVariablesShouldResolveVariables() {

        String endpointExpression = "/{param1}/{param2}/nonparam";

        Map<String, String> params = new HashMap<>();

        params.putIfAbsent( "param1", "value1" );

        params.putIfAbsent( "param2", "value2" );

        String expected = "/value1/value2/nonparam";

        assertThat( EndpointSupport.resolvePathVariables( endpointExpression, params ) ).isEqualTo( expected );

        params = new HashMap<>();

        params.putIfAbsent( "param2", "value2" );

        expected = "/param1/value2/nonparam";

        assertThat( EndpointSupport.resolvePathVariables( endpointExpression, params ) ).isEqualTo( expected );
    }


    @Test
    public void resolvePathVariablesShouldResolveVariablesWithEmptySpaces() {

        String endpointExpression = "/{param1}/{param2}/nonparam";

        Map<String, String> params = new HashMap<>();

        params.putIfAbsent( "param1", "value 1" );

        params.putIfAbsent( "param2", "value 2" );

        String expected = "/value%201/value%202/nonparam";

        assertThat( EndpointSupport.resolvePathVariables( endpointExpression, params ) ).isEqualTo( expected );

        //let's try another combination
        params = new HashMap<>();

        params.putIfAbsent( "param2", "value2" );

        expected = "/param1/value2/nonparam";

        assertThat( EndpointSupport.resolvePathVariables( endpointExpression, params ) ).isEqualTo( expected );

        //let's try another combination
        params = new HashMap<>();

        params.putIfAbsent( "param2", "value 2" );

        expected = "/param1/value%202/nonparam";

        assertThat( EndpointSupport.resolvePathVariables( endpointExpression, params ) ).isEqualTo( expected );

        //let's try a combination with null
        params = new HashMap<>();

        params.put( "param1", null );

        expected = "/null/param2/nonparam";

        assertThat( EndpointSupport.resolvePathVariables( endpointExpression, params ) ).isEqualTo( expected );
    }

}
