package com.innocv.selection_process.rest_exercise.service;


import com.innocv.selection_process.rest_exercise.model.domain.User;
import com.innocv.selection_process.rest_exercise.model.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;


/**
 * Defines a service layer above the model of users.
 *
 * @author INNOCV.
 */
@Service
@Transactional( readOnly = true )
public class UserService
{


    private final UserRepository userRepository;


    /**
     * Creates a user service equipped with the specified <code>userRepository</code>.
     *
     * @param userRepository  a user repository.
     */
    public UserService( UserRepository userRepository ) {

        this.userRepository = userRepository;
    }


    /**
     * Returns the current list of users present.
     *
     * @return the current list of users present.
     */
    public List< User > findAll() {

        return this.userRepository.findAll();
    }


    /**
     * Returns the user whose id is equal to the specified <code>userId</code>.
     *
     * <p>An empty optional is returned from this method if there is no user with the specified <code>userId</code></p>.
     *
     * @param userId  some user id.
     * @return  an optional user.
     */
    public Optional< User > findOne( Integer userId ) {

        Assert.notNull( userId, "userId must not be null" );

        Integer count = this.userRepository.countById( userId );

        if( count.intValue() == 0 ) {

            return Optional.empty();
        }

        return Optional.of( this.userRepository.findOne( userId ) );
    }


    /**
     * Removes the user whose id is equal to the specified <code>userId</code>.
     *
     * <p>An empty optional is returned from this method if there is no user with the specified <code>userId</code>, and,
     * an optional which contains the specified <code>userId</code> in case the user was actually deleted.
     *
     * @param userId  an id.
     * @return an optional user id.
     */
    @Transactional( readOnly = false )
    public Optional< Integer > removeUser( Integer userId ) {

        Assert.notNull( userId, "userId must not be null" );

        Integer count = this.userRepository.countById( userId );

        if( count.intValue() == 0 ) {

            return Optional.empty();
        }

        this.userRepository.delete( userId );

        return Optional.of( userId );
    }


    /**
     * Either creates or updates the specified <code>user</code>.
     *
     * @param user  some user.
     * @return  the user in question.
     */
    @Transactional( readOnly = false )
    public User createOrUpdateUser( User user ) {

        Assert.notNull( user, "user must not be null" );

        return this.userRepository.save( user );
    }


}
