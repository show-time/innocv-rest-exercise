package com.innocv.selection_process.rest_exercise.service;

import com.innocv.selection_process.rest_exercise.model.domain.User;
import com.innocv.selection_process.rest_exercise.model.repositories.UserRepository;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.BDDMockito.given;


/**
 * Tests for the {@link UserService} class.
 *
 * @author BBVA.
 */
@RunWith( SpringRunner.class )
public class UserServiceTests {


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private UserRepository userRepository;

    private UserService userService;


    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.userService = new UserService( this.userRepository );
    }


    @Test
    public void findAllShouldReturnAllUsers() {

        List< User > expected = Lists.newArrayList();

        given( this.userRepository.findAll() ).willReturn( expected );

        assertThat( this.userService.findAll() ).isEqualTo( expected );
    }


    @Test
    public void findOneWhenUserIdIsNullShouldThrowException() {

        this.thrown.expect( IllegalArgumentException.class );

        this.userService.findOne( null );
    }


    @Test
    public void findOneWhenUserIdDoesNotMatchShouldReturnEmptyOptional() {

        Integer userId = 1;

        User user = new User();

        user.setId( userId );

        given( this.userRepository.countById( userId ) ).willReturn( 1 );

        given( this.userRepository.findOne( userId ) ).willReturn( user );

        assertThat( this.userService.findOne( ++userId ).isPresent() ).isFalse();
    }


    @Test
    public void findOneWhenUserIdMatchesShouldReturnUser() {

        Integer userId = 1;

        User user = new User();

        user.setId( userId );

        given( this.userRepository.countById( userId ) ).willReturn( 1 );

        given( this.userRepository.findOne( userId ) ).willReturn( user );

        assertThat( this.userService.findOne( userId ).get() ).isEqualTo( user );
    }


    @Test
    public void createOrUpdateUserWhenUserIsNullShouldThrowException() {

        this.thrown.expect( IllegalArgumentException.class );

        this.userService.createOrUpdateUser( null );
    }


    @Test
    public void createOrUpdateUserShouldCreateOrUpdateUser() {

        User user = new User();

        given( this.userRepository.save( user ) ).willReturn( user );

        assertThat( this.userService.createOrUpdateUser( user ) ).isEqualTo( user );
    }


    @Test
    public void removeUserWhenUserIdIsNullShouldThrowException() {

        this.thrown.expect( IllegalArgumentException.class );

        this.userService.removeUser( null );
    }


    @Test
    public void removeUserWhenUserIdDoesNotMatchShouldThrowException() {

        Integer userId = 1;

        User user = new User();

        user.setId( userId );

        given( this.userRepository.countById( userId ) ).willReturn( 1 );

        assertThat( this.userService.removeUser( ++userId ).isPresent() ).isFalse();
    }


    @Test
    public void removeUserWhenUserIdMatchesShouldRemove() {

        Integer userId = 1;

        User user = new User();

        user.setId( userId );

        given( this.userRepository.countById( userId ) ).willReturn( 1 );

        assertThat( this.userService.removeUser( userId ).get() ).isEqualTo( userId );
    }

}
